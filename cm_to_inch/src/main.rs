// 引入用于 Lambda 函数的库
use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};

// 定义输入数据结构，接收厘米作为输入
#[derive(Deserialize)]
struct InputEvent {
    centimeters: f64,
}

// 定义输出数据结构，返回英尺和英寸
#[derive(Serialize)]
struct OutputEvent {
    feet: u32,  // 整数英尺
    inches: u32,  // 剩余的英寸
}

// 转换函数，将厘米转换为英尺和英寸
async fn convert(event: LambdaEvent<InputEvent>) -> Result<OutputEvent, Error> {
    let centimeters = event.payload.centimeters;
    let total_inches = centimeters * 0.393701;
    let feet = (total_inches / 12.0).floor() as u32;  // 获得整数英尺
    let inches = ((total_inches % 12.0).round() as u32).min(11);  // 获得剩余英寸，并确保不会超过11英寸

    Ok(OutputEvent { feet, inches })
}

// 主函数，设置 Lambda 运行时
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(convert);  // 包装转换函数以符合 Lambda 运行时要求
    lambda_runtime::run(func).await?;  // 运行 Lambda 函数
    Ok(())
}
