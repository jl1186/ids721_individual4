# IDS721 Individual Project 4 Rust AWS Lambda and Step Functions

## Requirements ☑️
Rust AWS Lambda function
Step Functions workflow coordinating Lambdas
Orchestrate data processing pipeline

## Grading Rubric ☑️
Rust Lambda Functionality (30 points)
Step Functions Workflow (30 points)
Data Processing Pipeline (20 points)
Documentation (10 points)
Demo Video (10 points)

## Lambda Functions

Here I created four different functions, which are one pair of Fahrenheit and Celsius translator, and one pair of centimeter to inches translator.

```
// fahrenheit to celsius
let celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
```

```
// celsius to fahrenheit
let fahrenheit = celsius * 9.0 / 5.0 + 32.0;
```

```
// cm to inch
let feet = (total_inches / 12.0).floor() as u32;
let inches = ((total_inches % 12.0).round() as u32).min(11);
```

```
// inch to cm
let total_inches = feet as f64 * 12.0 + inches as f64;
let centimeters = total_inches * 2.54;
```

## Step Functions

Here's the basic structure of the step function:

![Structure](./structure.png)

## Orchestrate data processing

So firstly the user should choose what type of unit you wanna choose, height or temperature. Then, you can continue to type in the value, and the step function will give back the translated value.