use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct InputEvent {
    feet: u32,
    inches: u32,
}

#[derive(Serialize)]
struct OutputEvent {
    centimeters: f64,
}

async fn convert(event: LambdaEvent<InputEvent>) -> Result<OutputEvent, Error> {
    let feet = event.payload.feet;
    let inches = event.payload.inches;
    let total_inches = feet as f64 * 12.0 + inches as f64;
    let centimeters = total_inches * 2.54; // 转换为厘米

    Ok(OutputEvent { centimeters })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(convert);  // 包装转换函数以符合 Lambda 运行时要求
    lambda_runtime::run(func).await?;  // 运行 Lambda 函数
    Ok(())
}