use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct InputEvent {
    fahrenheit: f64,
}

#[derive(Serialize)]
struct OutputEvent {
    celsius: f64,
}

async fn convert(event: LambdaEvent<InputEvent>) -> Result<OutputEvent, Error> {
    let fahrenheit = event.payload.fahrenheit;
    let celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
    Ok(OutputEvent { celsius })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(convert);
    lambda_runtime::run(func).await?;
    Ok(())
}
