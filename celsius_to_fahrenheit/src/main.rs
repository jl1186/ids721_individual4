use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct InputEvent {
    celsius: f64,
}

#[derive(Serialize)]
struct OutputEvent {
    fahrenheit: f64,
}

async fn convert(event: LambdaEvent<InputEvent>) -> Result<OutputEvent, Error> {
    let celsius = event.payload.celsius;
    let fahrenheit = celsius * 9.0 / 5.0 + 32.0;
    Ok(OutputEvent { fahrenheit })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(convert);
    lambda_runtime::run(func).await?;
    Ok(())
}
